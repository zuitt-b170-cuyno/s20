// JS Object
const address1 = {
    city: "Bacoor",
    province: "Cavite",
    country: "Philippines"
}

console.log(address1)

// JSON
// JSON is used for serializing different types into byte
// Serialization is a process of converting data into a series of bytes for easier transmission of information
const address2 = {
    "city": "Bacoor",
    "province": "Cavite",
    "country": "Philippines"
}

console.log(address2)

// JS object is quite similar JSON except that JSON has quotation mark enclosing both key and value
// JSON is not only exclusive to JS. It can also be used by other programming language.

// JSON Array
const cities = [
    {
        "city": "Bacoor",
        "province": "Cavite",
        "country": "Philippines"
    },
    {
        "city": "Ormoc",
        "province": "Leyte",
        "country": "Philippines"
    },
    {
        "city": "New York",
        "province": "New York",
        "country": "United States of America"
    }
]

console.log(cities)

// JSON Object
const batches = [
    {
        "batchName": "Batch A" 
    },
    {
        "batchName": "Batch B" 
    },
    {
        "batchName": "Batch C" 
    },
    {
        "batchName": "Batch D" 
    }
]

// JSON object contains method for parsing data
// Stringify - converts JSON object into JSON string
// Stringify with JSON Objects
console.log(batches)
console.log(JSON.stringify(batches))

// Stringify with JS Objects
const data = JSON.stringify({
    name: "Johnny",
    age: 31,
    address: {
        city: "Manila",
        country: "Philippines"
    }
})

console.log(data)

// const userDetail = JSON.stringify({
//     fname: prompt("Enter the first name:"),
//     lname: prompt("Enter the last name:"),
//     age: Number(prompt("Enter the age:")),
//     address: {
//         city: prompt("Enter the city:"),
//         country: prompt("Enter the country:"),
//         zipCode: prompt("Enter the zipcode:")
//     }
// })

// console.log(userDetail)

const batchJSON = `[
    {
        "batchName": "Batch A" 
    },
    {
        "batchName": "Batch B" 
    }
]`

// JSON Parse - converts JSON string into JS Objects
// Information is commonly sent to application in stringified JSON and then converted into objects
// This happens both for sending information to backend such as database and back to frontend
// When sending data, it is in the form of stringified JSON
console.log(batchJSON)
console.log(JSON.parse(batchJSON))
console.log(JSON.parse(data))

let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`

console.log(JSON.parse(stringifiedData))